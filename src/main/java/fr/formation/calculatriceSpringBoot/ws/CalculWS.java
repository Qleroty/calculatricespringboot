package fr.formation.calculatriceSpringBoot.ws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import fr.formation.calculatriceSpringBoot.bll.CalculService;

@RestController
@RequestMapping("/calculatrice")
public class CalculWS {

	@Autowired
	private CalculService service;
	
	@GetMapping("/add")
	public Integer addition(@RequestParam Integer a, @RequestParam Integer b) {
		return service.addition(a, b);
	}
}
